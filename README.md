# CLISTER - A Corpus for Semantic Textual Similarity in French Clinical Narratives

CLISTER is a Semantic Textual Similarity corpus in French constructed based on the CAS corpus (https://deft.limsi.fr/2020/), a French corpus with clinical cases. 

The CLISTER corpus containes 1,000 sentence pairs annotated with a similarity score on a scale of 0 to 5, 0 being the minimum similarity score and 5 being the maximum. 

### Access

To Do

### How to use

The corpus is separated in training and testing sets (files __train.tsv__ and __test.tsv__). Those files contain pairs of IDs, corresponding to the sentences in the CAS corpus, and the annotations. 

Once the CAS corpus is acquired, the script (get_sentences_from_ids.py) allows to build a json dictionary associating the IDs of the annotation file with the corresponding sentence in the corpus. It is then possible to use the generated .json file with the annotation file to have the full corpus. 
